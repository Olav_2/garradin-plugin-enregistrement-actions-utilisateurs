# Plugin d'enregistrement des actions utilisateurs pour Garradin

Ce plugin permet d'enregistrer et visualiser les principales actions des utilisateurs (création et édition de membres, envoi d'emails, connexions à l'espace membre, ...)

# Installation

Télécharger le fichier enregistrement_actions_utilisateurs.tar.gz et l'ajouter dans le répértoire garradin/plugins.

Installer le plugin via le menu "Configuration > Extensions" de Garradin.

# Utilisation

Les actions enregistrées sont consultables via le menu de Garradin : "Enregistrement des actions utilisateurs" (uniquement pour les utilisateurs ayant le droit d'administration sur la configuration Garradin)

