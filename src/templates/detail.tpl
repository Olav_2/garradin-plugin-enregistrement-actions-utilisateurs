{include file="admin/_head.tpl" title="Extension — %s"|args:$plugin.nom current="plugin_%s"|args:$plugin.id}

<p>Detail de l'action utilisateur numéro {$rowid}</p>

<br>


<dl class="describe">
	{foreach from=$detailsAction key="champs" item="valeur"}
		<dt>{$champs|escape}</dt>
		<dd>{$valeur|escape:raw}</dd>
	{/foreach}

</dl>

{include file="admin/_foot.tpl"}
