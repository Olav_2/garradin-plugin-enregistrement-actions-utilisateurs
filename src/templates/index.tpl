{include file="admin/_head.tpl" title="Extension — %s"|args:$plugin.nom current="plugin_%s"|args:$plugin.id js=1}

<p>Visualisation de l'historique des actions des utilisateurs.</p>
<br>


<form method="get" action="{$self_url}" class="shortFormLeft">
    <fieldset>
        <legend>Mot clef et période</legend>
        Doit contenir : <input type="text" name="kw" id="kw"  value="{$keyword}"/>
	Type d'action : 
	<select name="type_action" id="type_action">
            <option value="-- Tous" {if $current_type_action == "-- Tous"} selected="selected"{/if}>-- Tous</option>
        {foreach from=$types_actions item="type_action"}
            <option value="{$type_action}"{if $current_type_action == $type_action} selected="selected"{/if}>{$type_action}</option>
        {/foreach}
        </select>
	Nombre maximum de lignes affichées : 
	<input type="number" name="li" min="1" max="10000" id="li" value="{$row_limit}"/>
	<br>
        Date de début : <input type="date" name="deb" id="date_debut"  value="{$date_debut}"/><br>
        Date de fin : <input type="date" name="fin" id="date_fin"  value="{$date_fin}" />
        <input type="submit" value="Filtrer &rarr;" />
    </fieldset>
</form>





<table class="list">
	<thead class="userOrder">
		<tr>
			<td>date et heure</td>
			<td>numéro utilisateur</td>
			<td>identifiant utilisateur</td>
			<td>identité utilisateur</td>
			<td>action</td>	
			<td>résumé paramètres</td>
			<td></td>	
		<tr>
		
	</thead>

	<tbody>
		{foreach from=$liste item="action"}
		<tr>
			<td>
				{$action.timestamp_action|escape}
			</td>
			<td>	
				{$action.utilisateur_id|escape}			
			</td>
			<td>
				{$action.utilisateur_identifiant|escape}
			</td>
			<td>
				{$action.utilisateur_identite|escape}	
			</td>
			<td>
				{$action.type_action|escape}	
			</td>
			<td>
				{$action.resultat_action|escape}
			</td>
			<td class="actions">
				<a class="icn" href="detail.php?id={$action.rowid}" title="Détails">▶</a>
				
			</td>
		</tr>
		{/foreach}
	</tbody>
		
</table>

{if $limite_atteinte == "oui"}<p>Il y a davantage d'enregistrements que la limite ({$row_limit} enregistrements maximum).<br>
Choisissez des critères de recherche plus sélectifs ou augmentez cette limite.  
</p>{/if}

{include file="admin/_foot.tpl"}
