<?php

namespace Garradin\Plugin\enregistrement_actions_utilisateurs;

use Garradin\Plugin;
use Garradin\DB;
use Garradin\Config;
use Garradin\Membres;
use Garradin\Membres\Session;
use Garradin\Utils;
use Garradin\UserException;

class EnregistrementActions
{
	static public function enregistrerAction($type_action, $params)
	{
		$data = [];

		$data['timestamp_action'] = date("Y-m-d H:i:s");

		$data['type_action'] = $type_action;

		# récupération des infos de l'utilisateur qui a fait l'action
		$config = Config::getInstance();

		$champ_identifiant = $config->get('champ_identifiant');
		$champ_identite = $config->get('champ_identite');

		$session = Session::getInstance();

		try {
			$user = $session->getUser();

			$data['utilisateur_id'] = $user->id;
			$data['utilisateur_identifiant'] = $user->$champ_identifiant;
			$data['utilisateur_identite'] = $user->$champ_identite;
		
		} catch (\LogicException $e) {
			# si il n'y a pas d'utilisateur connecté
			# (cas du membre qui demande la réinitialisation de son mot de passe)
			$data['utilisateur_id'] = "-1";
			$data['utilisateur_identifiant'] = "Pas d'identifiant";
			$data['utilisateur_identite'] = "Demande de réinitialisation mot de passe";
		}

		if ($type_action == 'membre.nouveau' or $type_action == 'membre.edit') 
		{
			# sauvegarde en doublon de l'identifiant et de l'identite du membre modifié
			# dans des variables ad-hoc, au cas où le paramètrage des champs identifiant
			# ou identité serait modifié en cours de route
			$params['membre_identifiant'] = $params[$champ_identifiant];
			$params['membre_identite'] = $params[$champ_identite];
		}

		if ($type_action == 'membre.connexion') 
		{
			# les infos utilisateurs et session sont redondantes, pas besoin de les enregistrer
			$params = array();
		} 



		$data['resultat_action'] = json_encode($params, JSON_UNESCAPED_UNICODE);


		# écriture dans le fichier de log (format CSV)
		$plugin = new Plugin('enregistrement_actions_utilisateurs');
		$plugin_path = $plugin->path();
		unset($plugin); 


		# écriture dans la base de données sqlite
	        $db = DB::getInstance();

		$db->insert('plugin_actions_utilisateurs', $data);

	}

	static public function listActions($date_debut, $date_fin, $keyword, $type_action, $row_limit) 
	{
		# transformation des dates en timestamps
		$date_debut .= " 00:00:00";
		$date_fin .= " 23:59:00";
		$where = "where timestamp_action between datetime(:la_date_debut) and datetime(:la_date_fin) ";

		# on limite à une ligne de trop pour voir si on a atteint la limite ou pas
		$row_limit = $row_limit + 1;
		$limit = " limit :le_row_limit ";

		$arguments = ['la_date_debut' => $date_debut,
			'la_date_fin' => $date_fin,
			'le_row_limit' => strval($row_limit)
			];

		if ($keyword != "") 
		{
			$keyword_pour_like = "%".$keyword."%";

			$where .= " and ( cast(utilisateur_id as text) = :le_keyword ";
			$where .= " or utilisateur_identifiant like :le_keyword_pour_like ";
			$where .= " or utilisateur_identite like :le_keyword_pour_like ";
			$where .= " or resultat_action like :le_keyword_pour_like ";
			$where .= " ) ";

			$arguments['le_keyword'] = $keyword;
			$arguments['le_keyword_pour_like'] = $keyword_pour_like;

		}

		if ($type_action != '-- Tous')
		{
			$where .= " and type_action = :le_type_action ";
			$arguments['le_type_action'] = $type_action;
		}

		$db = DB::getInstance();
		$query = "select rowid, timestamp_action, utilisateur_id, utilisateur_identifiant, utilisateur_identite, type_action, resultat_action
 			from plugin_actions_utilisateurs ".$where.$limit;
		$list = $db->get($query,$arguments);


		foreach ($list as &$action) {
			$resultat_simplifie = self::simplifierResultat($action->{'type_action'}, $action->{'resultat_action'});			
			$action->{'resultat_action'} = $resultat_simplifie;
		}

		return $list;

	}

	static public function getDetailsAction($rowid)
	{
		$db = DB::getInstance();

		$query = "select timestamp_action, utilisateur_id, utilisateur_identifiant, utilisateur_identite, type_action, resultat_action
 			from plugin_actions_utilisateurs where rowid=:le_rowid";

		$data = $db->first($query, ['le_rowid' => $rowid]);

		if ($data == null) {
			return null;
		}

		$details = array();
		$details['Date et heure'] = $data->{'timestamp_action'};
		$details['Numéro utilisateur'] = $data->{'utilisateur_id'};
		$details['Identifiant de l\'utilisateur'] = $data->{'utilisateur_identifiant'};
		$details['Identité de l\'utilisateur'] = $data->{'utilisateur_identite'};
		$details['Type d\'action'] = $data->{'type_action'};


		$resultats = json_decode($data->{'resultat_action'});

		foreach ($resultats as $key=>$resultat) 
		{
			if (strcmp($key, 'recipients') != 0) {
				$resultat = str_replace("\n",'<br>',$resultat);
				$key = ucfirst($key);
				$details[$key] = $resultat;
			} else {
				$details[$key] = "";
				foreach ($resultat as $email=>$user_data) { 
					$details[$key] .= $email."<br>";
				}
			}
		}

		return $details;

	} 

	static private function simplifierResultat($type_action, $resultat_action) 
	{
		$resultats = json_decode($resultat_action);

		switch ($type_action) 
		{
			case 'membre.nouveau' :
			case 'membre.edit' :
				if (property_exists($resultats,'numero')) {
					$resultat_simplifie = "numéro = ".$resultats->{'numero'} . ", ";
				} else {
					$resultat_simplifie = "";
				}
				$resultat_simplifie .= "identifiant = ".$resultats->{'membre_identifiant'};
				$resultat_simplifie .= ", identité = ".$resultats->{'membre_identite'};
				break;
			case 'cotisation.ajout' :
				$resultat_simplifie = "numéro membre = ".$resultats->{'id_membre'};
				$resultat_simplifie .= ", identifiant = ".$resultats->{'membre_identifiant'};
				$resultat_simplifie .= ", identité = ".$resultats->{'membre_identite'};
				break;
			case 'email.envoi' :
				try 
				{
					# compatibilité avec les anciens envois d'emails unitaires des versions précédentes de Garradin
					$resultat_simplifie = "dest = ".$resultats->{'recipient'};			
					$resultat_simplifie .= ", objet = ".$resultats->{'subject'};
				}
				catch (\Exception $e) {
					
					if (count((array) $resultats->{'recipients'}) == 1) {
						# si un seul destinataire, on l'affiche		
						foreach ($resultats->{'recipients'} as $email_dest=>$email_dest_data) { 
							$resultat_simplifie = "dest = ".$email_dest;
						}
					}
					else {
						# sinon, on affiche le nombre de destinataires
						$resultat_simplifie = strval(count((array) $resultats->{'recipients'})) . " destinataires";
					}
					$resultat_simplifie .= ", objet = ".$resultats->{'subject'};
					
				}			
				break;
			default :
				$resultat_simplifie = $resultat_action;

		}

		return $resultat_simplifie;

	}

	# fonctions recevant les signaux :

	static public function nouveauMembre(array &$params)
	{
		self::enregistrerAction('membre.nouveau', $params);

		return false;
	}

	static public function editionMembre(array &$params)
	{
		self::enregistrerAction('membre.edit', $params);

		return false;
	}

	static public function suppressionMembre(array &$params)
	{
		self::enregistrerAction('membre.suppression', $params);

		return false;
	}

	static public function envoiEmail(array &$params)
	{
		self::enregistrerAction('email.envoi', $params);

		return false;
	}
	
	static public function connexion(array &$params)
	{
		self::enregistrerAction('membre.connexion', $params);

		return false;
	}		

	static public function getTypesActions()
	{
		return ['membre.nouveau', 'membre.connexion', 'membre.edit', 'membre.suppression', 'email.envoi'];
	}

}
