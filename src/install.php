<?php

namespace Garradin;

$db = DB::getInstance();

$db->exec(<<<EOF

    CREATE TABLE IF NOT EXISTS plugin_actions_utilisateurs
    (
	timestamp_action TEXT, 

	utilisateur_id INTEGER,
	utilisateur_identifiant TEXT,
	utilisateur_identite TEXT,
	
	type_action TEXT,
	resultat_action TEXT

    );

EOF
);

$plugin->registerSignal('membre.nouveau', 'Garradin\Plugin\enregistrement_actions_utilisateurs\EnregistrementActions::nouveauMembre');

$plugin->registerSignal('membre.edit', 'Garradin\Plugin\enregistrement_actions_utilisateurs\EnregistrementActions::editionMembre');

$plugin->registerSignal('membre.suppression', 'Garradin\Plugin\enregistrement_actions_utilisateurs\EnregistrementActions::suppressionMembre');

$plugin->registerSignal('email.queue.after', 'Garradin\Plugin\enregistrement_actions_utilisateurs\EnregistrementActions::envoiEmail');

$plugin->registerSignal('accueil.banniere', 'Garradin\Plugin\enregistrement_actions_utilisateurs\EnregistrementActions::connexion');



namespace Garradin\Plugin\enregistrement_actions_utilisateurs;

EnregistrementActions::enregistrerAction("plugin_enregistrement_actions.installation", []);

