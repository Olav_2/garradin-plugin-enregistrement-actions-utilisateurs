<?php

namespace Garradin;

require_once __DIR__ . '/_inc.php';

qv(['id' => 'required|numeric']);

$id = (int) qg('id');

$tpl->assign('rowid', $id);

use Garradin\Plugin\enregistrement_actions_utilisateurs\EnregistrementActions;


$detailsAction = EnregistrementActions::getDetailsAction($id);

$tpl->assign('detailsAction', $detailsAction);

if (!$detailsAction)
{
    throw new UserException("Cette action utilisateur n'existe pas.");
}

$tpl->display(PLUGIN_ROOT . '/templates/detail.tpl');


