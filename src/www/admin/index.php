<?php

namespace Garradin;

require_once __DIR__ . '/_inc.php';

use Garradin\Plugin\enregistrement_actions_utilisateurs\EnregistrementActions;

if (qg('deb'))
# filtre par date de début
{
	$date_debut = qg('deb');
} else {
	$date_debut = date('Y-m-01');
}
$tpl->assign('date_debut', $date_debut);

if (qg('fin'))
# filtre par date de fin
{
	$date_fin = qg('fin');
} else {
	$date_fin = date('Y-m-t');
}
$tpl->assign('date_fin', $date_fin);


if (qg('kw'))
# filtre par texte à rechercher
{
	$keyword = qg('kw');
} else {
	$keyword = "";
}
$tpl->assign('keyword', $keyword);


if (qg('li'))
# nombre maximum de lignes affichées
{
	$row_limit = qg('li');
} else {
	$row_limit = 500;
}
$tpl->assign('row_limit', $row_limit);


# filtre par type action
$types_actions = EnregistrementActions::getTypesActions();
$tpl->assign('types_actions', $types_actions);

$current_type_action = "-- Tous";
if (qg('type_action'))
{
	$current_type_action = qg('type_action');
}

$tpl->assign('current_type_action', $current_type_action);



$liste = EnregistrementActions::listActions($date_debut, $date_fin, $keyword, $current_type_action, $row_limit);

if (count($liste) > $row_limit)
{
	# au cas où on a atteint la limite de lignes, on enlève la derniere ligne
	array_pop($liste);
	$limite_atteinte = "oui";
} else {
	$limite_atteinte = "non";
}

$tpl->assign('limite_atteinte', $limite_atteinte);
$tpl->assign('liste', $liste);

$tpl->display(PLUGIN_ROOT . '/templates/index.tpl');


